FILES=agency.txt calendar.txt calendar_dates.txt fare_attributes.txt routes.txt stops.txt stop_times.txt trips.txt README CREDITS COPYING NEWS

all: google_transit.zip

google_transit.zip: $(FILES)
	zip google_transit $(FILES)

stop_times.txt trips.txt: time_table.xml
	python generate_trips_times.py

clean:
	rm -f google_transit.zip stop_times.txt trips.txt validation-results.html

validate: google_transit.zip
	gtfs-feedvalidator google_transit.zip

schedule: google_transit.zip
	gtfs-schedule_viewer --key $$(cat maps.key) --feed_filename google_transit.zip
