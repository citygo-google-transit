#!/usr/bin/python
###
# Copyright (C) 2008  Ian Weller <ianweller@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
###

from xml.dom import minidom
from csv import DictWriter

def time_addition(time, addthis):
    addthis = int(addthis)
    (hours, minutes) = time.split(':')
    hours = int(hours)
    minutes = int(minutes) + addthis
    while minutes >= 60:
        minutes = minutes - 60
        hours = hours + 1
    if hours < 10:
        hours = "0" + str(hours)
    else:
        hours = str(hours)
    if minutes < 10:
        minutes = "0" + str(minutes)
    else:
        minutes = str(minutes)
    return "%s:%s" % (hours, minutes)

if __name__ == "__main__":
    trips = []
    trips.append(["route_id", "service_id", "trip_id", "direction_id",
                  "block_id"])
    stop_times = []
    stop_times.append(["trip_id", "arrival_time", "departure_time", "stop_id",
                       "stop_sequence"])
    dom = minidom.parse("time_table.xml")
    time_table = dom.documentElement
    for route in time_table.getElementsByTagName("route"):
        route_id = route.getAttribute("id")
        direction_id = route.getAttribute("direction")
        stops = route.getElementsByTagName("stops")[0]
        stops = stops.getElementsByTagName("stop")
        for schedule in route.getElementsByTagName("start_times"):
            service_id = schedule.getAttribute("schedule")
            for time in schedule.getElementsByTagName("start"):
                start_time = time.getAttribute("time")
                block_id = time.getAttribute("block")
                late = time.hasAttribute("late")
                trip_id = ''.join([route_id, direction_id, service_id,
                                   start_time.replace(':', '')])
                trips.append([route_id, service_id, trip_id, direction_id,
                              block_id])
                if not late:
                    trips.append([route_id, service_id+'_EARLY',
                                  trip_id+'_EARLY', direction_id, block_id])
                stop_sequence = 0
                for stop in stops:
                    stop_sequence = stop_sequence + 1
                    stop_id = stop.getAttribute("id")
                    if stop.hasAttribute("time"):
                        time = stop.getAttribute("time")
                        arrival_time = time_addition(start_time, time)
                        arrival_time = arrival_time + ":00"
                        departure_time = arrival_time
                    else:
                        (arrival_time, departure_time) = ('', '')
                    stop_times.append([trip_id, arrival_time, departure_time,
                                       stop_id, stop_sequence])
                    if not late:
                        stop_times.append([trip_id+'_EARLY', arrival_time,
                                           departure_time, stop_id,
                                           stop_sequence])
    trips_d = []
    for trip in trips:
        that = {}
        that["route_id"] = trip[0]
        that["service_id"] = trip[1]
        that["trip_id"] = trip[2]
        that["direction_id"] = trip[3]
        that["block_id"] = trip[4]
        trips_d.append(that)
    trips_f = file('trips.txt', 'w')
    trips_w = DictWriter(trips_f, trips[0])
    trips_w.writerows(trips_d)
    trips_f.close()
    stops_d = []
    for stop in stop_times:
        that = {}
        that["trip_id"] = stop[0]
        that["arrival_time"] = stop[1]
        that["departure_time"] = stop[2]
        that["stop_id"] = stop[3]
        that["stop_sequence"] = stop[4]
        stops_d.append(that)
    stops_f = file('stop_times.txt', 'w')
    stops_w = DictWriter(stops_f, stop_times[0])
    stops_w.writerows(stops_d)
    stops_f.close()
